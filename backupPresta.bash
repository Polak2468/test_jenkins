#!/bin/bash

#You can create /root/.my.cnf and add:
#[mysqldump]
#user=root
#password=PASSWORD
#With this you can remove the password and user for mysqldump from your script
#then without console warnings
#add to cron i.e. --> 0 3 * * * /root/backupPresta
Info () {
        echo -e "\e[1;34m$1\e[0m"
}

Success () {
        echo -e "\e[1;32m$1\e[0m"
}

Important () {
        echo -e "\e[1;33m$1\e[0m"
}

DATE=$(date +"%d-%m-%Y")
DIRECTORY="/var/backups/presta_multistore/"
MYSQLDUMP=/usr/bin/mysqldump

#need create .credentials file in proper directory contains data in form -> user,password
MYSQL_USER=$(awk 'BEGIN {FS=","} {print $1}' ~/.credentials)
MYSQL_PASSWORD=$(awk 'BEGIN {FS=","} {print $2}' ~/.credentials)

#files will be stored in this place and maybe rsync somewhere
cd $DIRECTORY

#backup the database, each in own file
databases=$(mysql --user=$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -E "presta_multistore")
for database in $databases
do
	Info "I am doing backup of $database."
    echo
    $MYSQLDUMP --force --log-error=error_file.log --user=$MYSQL_USER -p$MYSQL_PASSWORD --databases $database > $database"_"$DATE.sql

    #here need to archive and compress each file
    Important "Now I am going to create zipped archives to save some disk space and remove not necessary files. Please, give me a moment. Working on $database. :)"
    echo
    tar -czvf $database"_"$DATE.sql.tar.gz $database"_"$DATE.sql >/dev/null 2>&1
done

#make the backup files readable only by root
/bin/chmod 600 *.tar.gz

#delete unnecessary dumped db files
rm *.sql

#backup multistore to $DIRECTORY
cd /var/www
tar -czvf multistore"_"$DATE.tar.gz multistore >/dev/null 2>&1
mv multistore"_"$DATE.tar.gz $DIRECTORY

Info "Open path $DIRECTORY to see details."

#send data to remote Storage - rsync move whole $DIRECTORY to new place, no need to create it
rsync -az --delete -e ssh $DIRECTORY backup_user@synology:/var/services/homes/backup/presta_multistore

#mail admin about errors [ if file exists && ! empty ]
cd $DIRECTORY
if [[ -s "error_file.log" ]]
then
	echo "There were some errors during DB backup on the server $(hostname). You can check the log file attached to message." | mutt -s "Report - backup from day $(date +%y-%m-%d-%H:%M:%S)" mail-notify@example.com -a "$DIRECTORY/error_file.log"
	rm "error_file.log"
	touch "error_file.log"
fi

#array contains all files in $DIRECTORY
var=($(ls))
	
#remove them
echo -e "\e[38;5;196mRemoving these files from $DIRECTORY --> ${var[*]}.\e[39m"
rm -r ${var[*]}
echo -e "\e[38;5;40mRemoved with success.\e[39m"
	
#release some memory used to allocate for array variable
#unset var

echo
Success "Finished doing backup."